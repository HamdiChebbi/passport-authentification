var mongoose = require('mongoose'); // charger mongoose
var Schema = mongoose.Schema; // initialiser le schema 

var passportLocalMongoose = require('passport-local-mongoose'); // 

var Account = new Schema({

 username: String,

 password: String

});

Account.plugin(passportLocalMongoose);

module.exports = mongoose.model('Account', Account);